package com.akarepis.transfers.models;

public enum TransactionStatus {
    PENDING,
    PROCESSED
}
