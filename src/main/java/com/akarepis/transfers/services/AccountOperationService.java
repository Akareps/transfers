package com.akarepis.transfers.services;

import com.akarepis.transfers.models.Account;
import com.akarepis.transfers.models.Transaction;
import com.akarepis.transfers.models.TransactionStatus;
import com.akarepis.transfers.repositories.AccountRepository;
import com.akarepis.transfers.repositories.TransactionRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.beans.Transient;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class AccountOperationService {

    private AccountRepository accountRepository;
    private TransactionRepository transactionRepository;

    public AccountOperationService(AccountRepository accountRepository, TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    public void createAccount(Map<Object,Object> payload) {
        Account account = new Account();
        account.setAccountNumber((String) payload.get("accountNumber"));
        account.setAvailableBalance(BigDecimal.valueOf((Double) payload.get("balance")));
        account.setAccountBalance(BigDecimal.valueOf((Double) payload.get("balance")));
        account.setName((String) payload.get("name"));
        accountRepository.save(account);
    }
    public List<Account> fetchAccounts() {
        return accountRepository.findAll();
    }

    public void syncTheAccounts() {
        List<Transaction> pendingTransactions = transactionRepository.findByTransactionStatus(TransactionStatus.PENDING);
        pendingTransactions.forEach(pendingTransaction -> {

            pendingTransaction.setTransactionStatus(TransactionStatus.PROCESSED);
            transactionRepository.save(pendingTransaction);

            String source = pendingTransaction.getSourceNumber();
            Account sourceAccount = accountRepository.findByAccountNumber(source);
            sourceAccount.setAccountBalance(sourceAccount.getAvailableBalance());
            accountRepository.save(sourceAccount);

            String target = pendingTransaction.getTargetNumber();
            Account targetAccount = accountRepository.findByAccountNumber(target);
            targetAccount.setAccountBalance(targetAccount.getAvailableBalance());
            accountRepository.save(targetAccount);
        });
    }

    @PostConstruct
    public void init() {
        Account account = new Account();
        account.setAccountNumber("GR12345678901234");
        account.setAvailableBalance(BigDecimal.valueOf(500135000.0));
        account.setAccountBalance(BigDecimal.valueOf(500135000.0));
        account.setName("Alafouzos Ioannis");
        accountRepository.save(account);

        Account account2 = new Account();
        account2.setAccountNumber("GR12345678901235");
        account2.setAvailableBalance(BigDecimal.valueOf(400135000.0));
        account2.setAccountBalance(BigDecimal.valueOf(400135000.0));
        account2.setName("Afragouzos giannakis");
        accountRepository.save(account2);
    }

}
