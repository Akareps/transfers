package com.akarepis.transfers.services;

import com.akarepis.transfers.models.Transaction;
import com.akarepis.transfers.repositories.AccountRepository;
import com.akarepis.transfers.repositories.TransactionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class FetchClientTransactionsService {
    private AccountRepository accountRepository;
    private TransactionRepository transactionRepository;

    public FetchClientTransactionsService (AccountRepository accountRepository, TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    public List <Transaction> fetchAccountTransactionsService(Map<String,String> payload) {
        String sourceNumber = payload.get("sourceNumber");
        return transactionRepository.findBySourceNumberOrTargetNumberOrderBySourceNumber(sourceNumber,sourceNumber);
    }
}
