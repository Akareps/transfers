package com.akarepis.transfers.services;

import com.akarepis.transfers.models.Account;
import com.akarepis.transfers.models.Transaction;
import com.akarepis.transfers.models.TransactionStatus;
import com.akarepis.transfers.repositories.AccountRepository;
import com.akarepis.transfers.repositories.TransactionRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class TransactionOperationService {
    private AccountRepository accountRepository;
    private TransactionRepository transactionRepository;

    public TransactionOperationService (AccountRepository accountRepository, TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    @Transactional
    public void createTransaction(Map<Object,Object> payload) {
        Transaction transaction = new Transaction();
        transaction.setSourceNumber((String) payload.get("sourceNumber"));
        transaction.setTargetNumber((String) payload.get("targetNumber"));
        BigDecimal amount = (BigDecimal.valueOf((Double) payload.get("amount")));
        transaction.setAmount(amount);
        transaction.setDate(LocalDateTime.now());
        transaction.setTransactionStatus(TransactionStatus.PENDING);
        transactionRepository.save(transaction);

        Account sourceAccount = accountRepository.findByAccountNumber((String) payload.get("sourceNumber"));
        BigDecimal sourceAccountBalance = sourceAccount.getAvailableBalance();
        sourceAccountBalance = sourceAccountBalance.subtract(amount);
        sourceAccount.setAvailableBalance(sourceAccountBalance);
        if (sourceAccount.getAvailableBalance().compareTo(BigDecimal.ZERO) < 0)
            throw new RuntimeException("not available balance");
        accountRepository.save(sourceAccount);

        Account targetAccount = accountRepository.findByAccountNumber((String) payload.get("targetNumber"));
        BigDecimal targetAccountBalance = targetAccount.getAvailableBalance();
        targetAccountBalance = targetAccountBalance.add(amount);
        targetAccount.setAvailableBalance((targetAccountBalance));
        accountRepository.save((targetAccount));
    }

    public List<Transaction> fetchTransactions() {
        return transactionRepository.findAll();
    }
}
