package com.akarepis.transfers.repositories;

import com.akarepis.transfers.models.Transaction;
import com.akarepis.transfers.models.TransactionStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository <Transaction,Long> {
    List<Transaction> findByTransactionStatus(TransactionStatus transactionStatus);
    List<Transaction> findBySourceNumberOrTargetNumberOrderBySourceNumber(String sourceNumber, String targetNumber);
}
