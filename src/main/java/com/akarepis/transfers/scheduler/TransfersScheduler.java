package com.akarepis.transfers.scheduler;

import com.akarepis.transfers.services.AccountOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TransfersScheduler {
    @Autowired
    private AccountOperationService accountOperationService;
    @Scheduled(fixedRate = 1000*15)
    void batchTime() {
        accountOperationService.syncTheAccounts();
    }
}
