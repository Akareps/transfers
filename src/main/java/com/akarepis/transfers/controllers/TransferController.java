package com.akarepis.transfers.controllers;

import com.akarepis.transfers.models.Account;
import com.akarepis.transfers.models.Transaction;
import com.akarepis.transfers.repositories.AccountRepository;
import com.akarepis.transfers.repositories.TransactionRepository;
import com.akarepis.transfers.services.FetchClientTransactionsService;
import com.akarepis.transfers.services.TransactionOperationService;
import com.akarepis.transfers.services.AccountOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/transfers")
public class TransferController {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    AccountOperationService accountOperationService;

    @Autowired
    FetchClientTransactionsService fetchClientTransactionsService;

    @Autowired
    TransactionOperationService transactionOperationService;

    public TransferController (AccountOperationService accountOperationService) {
        this.accountOperationService = accountOperationService;
    }

    @PostMapping("/createAccount")
    public void createNewAccount(@RequestBody Map<Object,Object> payload) {
        accountOperationService.createAccount(payload);
    }

    @GetMapping("/getAccounts")
    public List<Account> fetchAccounts() {
        return accountOperationService.fetchAccounts();
    }

    @PostMapping("/createTransaction")
    public void createNewTransaction(@RequestBody Map<Object,Object> payload) {
        transactionOperationService.createTransaction(payload);
    }

    @GetMapping("/getTransactions")
    public List<Transaction> fetchTransactions() {
        return transactionOperationService.fetchTransactions();
    }

    @GetMapping("/getAccountTransactions")
    public List<Transaction> fetchAccountTransactions(@RequestBody Map<String,String> payload) {
        return fetchClientTransactionsService.fetchAccountTransactionsService(payload);
    }

}
