package com.akarepis.transfers.services;

import com.akarepis.transfers.models.Account;
import com.akarepis.transfers.models.Transaction;
import com.akarepis.transfers.models.TransactionStatus;
import com.akarepis.transfers.repositories.AccountRepository;
import com.akarepis.transfers.repositories.TransactionRepository;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

public class TransferOperationServiceTest {
    private AccountRepository accountRepositoryMock= mock(AccountRepository.class);
    private TransactionRepository transactionRepositoryMock = mock(TransactionRepository.class);
    private AccountOperationService accountOperationService = new AccountOperationService(accountRepositoryMock,transactionRepositoryMock);
    private TransactionOperationService transactionOperationService = new TransactionOperationService(accountRepositoryMock,transactionRepositoryMock);

    @Test
    public void testCreateAccount() {
        Map<Object,Object> payload = new HashMap<>();
        payload.put("accountNumber","GR12345678901234");
        payload.put("balance",500135000.0);
        payload.put("name","Ioannis afragkouzos");

        accountOperationService.createAccount(payload);

        Account account = new Account();
        account.setAccountNumber("GR12345678901234");
        account.setAvailableBalance(BigDecimal.valueOf(500135000.0));
        account.setAccountBalance(BigDecimal.valueOf(500135000.0));
        account.setName("Ioannis afragkouzos");
        verify(accountRepositoryMock,times(1)).save(account);
    }

    @Test
    public void testCreateTransaction() {
        Account account1 = new Account();
        account1.setAccountBalance(BigDecimal.valueOf(500135000.0));
        account1.setAvailableBalance(BigDecimal.valueOf(500135000.0));
        account1.setAccountNumber("GR12345678901234");
        when(accountRepositoryMock.findByAccountNumber("GR12345678901234")).thenReturn(account1);

        Account account2 = new Account();
        account2.setAccountBalance(BigDecimal.valueOf(500135000.0));
        account2.setAvailableBalance(BigDecimal.valueOf(500135000.0));
        account2.setAccountNumber("GR12345678901235");
        when(accountRepositoryMock.findByAccountNumber("GR12345678901235")).thenReturn(account2);

        Map<Object,Object> payload = new HashMap<>();
        payload.put("sourceNumber","GR12345678901234");
        payload.put("targetNumber","GR12345678901235");
        payload.put("amount",100.0);


        transactionOperationService.createTransaction(payload);


        Transaction transaction = new Transaction();
        transaction.setSourceNumber("GR12345678901234");
        transaction.setTargetNumber("GR12345678901235");
        transaction.setAmount(BigDecimal.valueOf(100.0));
        transaction.setTransactionStatus(TransactionStatus.PENDING);
        transaction.setDate(LocalDateTime.now());
        verify(transactionRepositoryMock,times(1)).save(transaction);
    }

    @Test
    public void testSyncTheAccounts() {
        Transaction transaction = new Transaction();
        transaction.setAmount(BigDecimal.valueOf(1000.0));
        transaction.setDate(LocalDateTime.now());
        transaction.setSourceNumber("GR12345678901234");
        transaction.setTargetNumber("GR12345678901235");
        transaction.setTransactionStatus(TransactionStatus.PENDING);

        Transaction transaction2= new Transaction();
        transaction2.setAmount(BigDecimal.valueOf(100.0));
        transaction2.setDate(LocalDateTime.now());
        transaction2.setSourceNumber("GR12345678901235");
        transaction2.setTargetNumber("GR12345678901234");
        transaction2.setTransactionStatus(TransactionStatus.PENDING);

        Account account1 = new Account();
        account1.setAccountBalance(BigDecimal.valueOf(500135000.0));
        account1.setAvailableBalance(BigDecimal.valueOf(500135000.0));
        account1.setAccountNumber("GR12345678901234");
        when(accountRepositoryMock.findByAccountNumber("GR12345678901234")).thenReturn(account1);

        Account account2 = new Account();
        account2.setAccountBalance(BigDecimal.valueOf(500135000.0));
        account2.setAvailableBalance(BigDecimal.valueOf(500135000.0));
        account2.setAccountNumber("GR12345678901235");
        when(accountRepositoryMock.findByAccountNumber("GR12345678901235")).thenReturn(account2);

        List <Transaction> pendingTransactions = new ArrayList<>();
        pendingTransactions.add(transaction);
        pendingTransactions.add(transaction2);
        when(transactionRepositoryMock.findByTransactionStatus(TransactionStatus.PENDING)).thenReturn(pendingTransactions);


        accountOperationService.syncTheAccounts();


        verify(transactionRepositoryMock,times(1)).save(transaction);
        verify(transactionRepositoryMock,times(1)).save(transaction2);
        verify(accountRepositoryMock,times(2)).save(account1);
        verify(accountRepositoryMock,times(2)).save(account2);
    }
}